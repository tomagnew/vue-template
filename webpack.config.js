// webpack.config.js
var path = require("path"); // native node module for composing file paths
var webpack = require("webpack");
// var validate = require('webpack-validator'); // depracated for webpack 2
var HtmlWebpackPlugin = require("html-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
//var stripDebug = false; // flag for strip-loader
const IS_DEV = false;
module.exports = {
    context: path.join(__dirname), // points to site root
    entry: { // multiple entry points go as keys within this object
        // index: path.join(__dirname, 'src/index'),
        index: ['./src/main'],
        //html: './src/index.html',
        // test: path.join(__dirname, 'src/test.js')
        // css: ['./src/main.css', './src/extra.css', './src/mySass.scss']
    },
    // entry: "./src/index.ts", // single entry point
    output: {
        path: path.join(__dirname, 'build'),
        filename: "[name].bundle.js"
        //chunkFilename: "[id].js"
    },
    devtool: "#eval-source-map", //source-map",
    resolve: {
        extensions: [".ts", ".js"],
        alias: {
            'vue$': 'vue/dist/vue'
        }
    },
    module: {
        rules: [
            // Vue.js
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    // vue-loader options go here
                },
                exclude: /node_modules/
            },
            // javascript - only needed to transpile using Babel
            {
                test: /.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /.ts$/,
                loader: 'awesome-typescript-loader',
                exclude: /node_modules/
            }

            // use these 2 for inlining css/sass into Head of DOM
            // ,{
            //     test: /\.css/,
            //     use: [
            //         'style-loader',
            //         'css-loader'
            //     ]
            // },
            // {
            //     test: /\.(scss)|(sass)/,
            //     use: [
            //         'style-loader',
            //         'css-loader',
            //         'sass-loader'
            //     ]
            // }

            // use these 2 for creating separate css/sass files.
            // Index.html will automatically include link tags in Head of DOM
            , {
                test: /\.css/,
                use: ExtractTextPlugin.extract({
                    // fallback: 'style-loader',
                    use: 'css-loader'
                })
            }, {
                test: /\.scss/,
                use: ExtractTextPlugin.extract({
                    // fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            // Images
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            },
            // Bootstrap 3
            { test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports-loader?jQuery=jquery' },

            // Bootstrap 4
            { test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/, loader: 'imports-loader?jQuery=jquery' },
            { test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000' },
            { test: /\.(ttf|eot)$/, loader: 'file-loader' }
        ]
    },
    // target:'node',
    plugins: [
        // new webpack.optimize.UglifyJsPlugin({
        //     // minimize: false,
        //     compress: false,
        //     // compress: {
        //     //     warnings: false
        //     // },
        //     mangle: false
        // }),
        new HtmlWebpackPlugin({ // dynamically creates index.html off of template file
            filename: 'index.html',
            title: "group project",
            template: "src/index.html",
            //cache: false
        }),
        new HtmlWebpackPlugin({ // dynamically creates index.html off of template file
            filename: 'bootstrap.html',
            title: "Bootstrap Sample",
            template: "src/bootstrap.html",
            //cache: false
        }),
        new ExtractTextPlugin("styles.css")
    ],
    devServer: {
        historyApiFallback: true, // Allow for pretty URL's (without hash)
        contentBase: "build", // default directory which dev server will serve (index.html)
        // inline: true,
        // hot: true,
        open: true,
        publicPath: "/",
        // stats: "errors-only", // don't list out all the extra server files in cmd line'
        stats: {
            colors: true,
            reasons: true,
            chunks: false
        } // optimize: "minimize"
    }
};

// if (process.env.NODE_ENV === 'production') {
//   module.exports.devtool = '#source-map'
//   // http://vue-loader.vuejs.org/en/workflow/production.html
//   module.exports.plugins = (module.exports.plugins || []).concat([
//     new webpack.DefinePlugin({
//       'process.env': {
//         NODE_ENV: '"production"'
//       }
//     }),
//     new webpack.optimize.UglifyJsPlugin({
//       compress: {
//         warnings: false
//       }
//     }),
//     new webpack.LoaderOptionsPlugin({
//       minimize: true
//     })
//   ])
// }
