console.log('Mocha unit tests running!');
// This file is entry point for test runner

// run specific tests only
// ACCESS MODULE
// var testsContext = require.context("../src/modules/access/", true, /.spec.(t|j)s$/);

// MEMBER_AREA MODULE
// var testsContext = require.context("../src/modules/member_area/", true, /.spec.(t|j)s$/);

// LOGIN_SCREEN MODULE
// var testsContext = require.context("../src/modules/login_screen/", true, /.spec.(t|j)s$/);

// MODULE_TEMPLATE
var testsContext = require.context("../src/modules/module_template/", true, /.spec.(t|j)s$/);

// Run all tests
// look for all specs (test files ending in .spec.js) and run them
// var testsContext = require.context("../src/", true, /.spec.(t|j)s$/);

// Loop thru test files specified
testsContext.keys().forEach(testsContext);