// karma.conf.js  --  karma configuration

// if you import your existing 'webpack.config.js' setup here,
// be sure to read the note about 'entry' below.
var webpackConfig = require('./webpack.config.js');
var path = require('path');
module.exports = function (config) {
    config.set({
        // ... normal karma configuration

        files: [
            // all files ending in "_test"
            // 'test/*_test.js',
            // 'test/**/*_test.js',
            // 'src/**/*_test.js'
            // {pattern:'src/**/*.spec.ts'}
            'test/index.js'
            // 'test/testAnimals.ts'
            // each file acts as entry point for the webpack configuration
        ],
        browsers: ['Chrome'], // 'Chrome_with_debugging'
        // frameworks: ['mocha', 'karma-typescript'],
        // customLaunchers: {
        //     Chrome_with_debugging: {
        //         base: 'Chrome',
        //         chromeDataDir: path.resolve(__dirname, '.chrome')
        //     }
        // },
        frameworks: ['mocha'],
        preprocessors: {
            // add webpack as preprocessor
            //             'src/modules/module_1/': ['webpack'],
            // 'test/*_test.js': ['webpack'],
            // 'test/**/*_test.js': ['webpack'],
            // 'src/**/*_test.js': ['webpack']
            // 'src/**/*.ts': ['karma-typescript','webpack']
            'test/index.js': ['webpack','sourcemap']
            // ,"test/testAnimals.ts": ["karma-typescript"]
        },
        // karmaTypescriptConfig: {
        //     compilerOptions: {
        //         module: "commonjs"
        //     },
        //     tsconfig: "./tsconfig.json",
        // },
        reporters: ['html', 'spec'], // 'karma-typescript'
        htmlReporter: { // for karma-html-reporter
            outputDir: 'test', // where to put the reports 
            templatePath: null, // set if you moved jasmine_template.html
            focusOnFailures: false, // reports show failures on start
            namedFiles: true, // name files instead of creating sub-directories
            pageTitle: "unit tests", // page title for reports; browser info by default
            urlFriendlyName: false, // simply replaces spaces with _ for files/dirs
            reportName: 'unit-tests', // report summary filename; browser info by default


            // experimental
            preserveDescribeNesting: false, // folded suites stay folded 
            foldAll: false, // reports start folded (only with preserveDescribeNesting)
        },
        // htmlReporter: { // for karma-htmlfile-reporter
        //     outputFile: 'test/units.html',

        //     // Optional
        //     pageTitle: 'Unit Tests',
        //     subPageTitle: 'A sample project description',
        //     groupSuites: true,
        //     useCompactStyle: true,
        //     useLegacyStyle: false
        // },
        specReporter: {
            maxLogLines: 5,             // limit number of lines logged per test 
            suppressErrorSummary: false, // do not print error summary 
            suppressFailed: false,      // do not print information about failed tests 
            suppressPassed: false,      // do not print information about passed tests 
            suppressSkipped: true,      // do not print information about skipped tests 
            showSpecTiming: false,      // print the time elapsed for each spec 
            failFast: false              // test would finish with error when a first fail occurs.  
        },
        webpack: {
            // you don't need to specify the entry option because
            // karma watches the test entry points
            // webpack watches dependencies

            // ... remainder of webpack configuration (or import)
            module: webpackConfig.module,
            resolve: webpackConfig.resolve,
            devtool: 'inline-source-map'
        },

        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i.e.
            noInfo: true,
            // and use stats to turn off verbose output
            stats: {
                // options i.e. 
                chunks: false
            }
        },
        plugins: [
            require("karma-mocha"),
            require("karma-webpack"),
            // require("karma-typescript"),
            require("karma-chrome-launcher"),
            require("karma-spec-reporter")
            // , require("karma-htmlfile-reporter")
            ,require("karma-html-reporter")
            ,require("karma-sourcemap-loader")
        ]

    });
};