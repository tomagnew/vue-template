Standard Webpack template which includes:

TypeScript for static type checking & transpiling

Babel for javascript transpiling

Css/Sass for CSS

Karma/Mocha/Chai for testing

***

Jun 4, 2017 | 2:50 PM
[ Immanuel Sun ]:
added bootstrap-loader with bootstrap 3 working. Please use `bootstap-sample.html` for preview.
