import $ from 'jquery';
import memberAreaHtml from 'html-loader!./member_content.html';

// load member area content to hidden div
$("#member_area").html(memberAreaHtml).hide();

// exports function which shows member area if user logged in, hides otherwise
export default (access) => {
    if (access.loggedIn) {
        // $("#member_area").show();
        $("#member").text(`Logged in as: ${access.currentUser.name}`);
                $("#login2").fadeOut(()=>$("#member_area").fadeIn());

    }
    else {
        // $("#member_area").hide();
        $("#member_area").fadeOut(()=>$("#login2").fadeIn());
        
    }
}