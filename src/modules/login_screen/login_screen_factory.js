import $ from 'jquery';
export default ({ loginHtml, access, showMemberArea }) => {
    return {
        btnLogin_click(username, password) {
            let response = access.login(username, password);
            console.log(response);
            response.done((data) => {
                console.log('access', access);
                showMemberArea(access);
            });
        },
        btnLogout_click() {
            access.logout();
            showMemberArea(access);
        },
        init() {
            console.log('login_screen init called...');

            // load login_screen html to #login area in main form
            $("#login").html(loginHtml);

            // assign login button click handler
            $("#btnLogin").on("click", () => {
                var username = $("#username").val();
                var password = $("#password").val();
                console.log("username", username);
                console.log("password", password);
                this.btnLogin_click(username, password);
            });

            // assign logout button click handler
            $("#btnLogout").on("click", () => {
                this.btnLogout_click();
            });
        }
    }
};