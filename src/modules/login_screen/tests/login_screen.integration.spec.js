var assert = require('chai').assert;  // use chai 'assert' assertion library
import $ from 'jquery';
import login_screen from '../index'; // run 'login_screen' module for side effects
global.$ = $;
// arrange
// $(function () {
var $login = $('<div>', { id: 'login' });
$('body').prepend($login);
console.log($('#login'));
// act
login_screen.init();
// });


describe('Login Screen module integration tests:', () => {
    beforeEach(function () {
        // runs before each test in this block
    });
    describe('Login form should contain certain elements', () => {
        // assert
        it('page should contain input with id = "username"', () => {
            // act
            var $username = $('#username');
            console.log('$username', $username)
            assert.notEqual($username.length, 0);
        });
        it('page should contain input with id = "password"', () => {
            // act
            var $password = $('#password');
            console.log('$username', $password)
            assert.notEqual($password.length, 0);
        });
        it('page should contain button with id = "btnLogin"', () => {
            // act
            var $btnLogin = $('#btnLogin');
            console.log('$btnLogin', $btnLogin)
            assert.notEqual($btnLogin.length, 0);
        });
        it('page should contain button with id = "btnLogout"', () => {
            // act
            var $btnLogout = $('#btnLogout');
            console.log('$btnLogout', $btnLogout)
            assert.notEqual($btnLogout.length, 0);
        });
    });

    // describe('Button click handlers', () => {
    //     // arrange
    //     $('#username').val("tom");
    //     $('#password').val("pwdtom");
    //     it('Clicking login button should call login method with username & password', () => {
    //         // act
    //         $('#btnLogin').click();
    //         // assert
    //         assert.equal(true, false);
    //     });
    // });
});