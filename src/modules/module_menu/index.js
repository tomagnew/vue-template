// import dependencies
import $ from 'jquery';
import '../module_home';
import '../module_about';
import '../module_contact';
import views from '../module_views';
// module setup logic

const pages = {
    home: {
        menuLink: 'home',
        viewId: '#home_page',
    },
    about: {
        menuLink: 'about',
        viewId: '#about_page',
    },
    contact: {
        menuLink: 'contact',
        viewId: '#contact_page',
    },
    members: {
        menuLink: 'members',
        viewId: '#members_page',
        setup() { $('#login').fadeIn(); }
    }
}

// export module api
export default {
    init() {
        for (var page in pages) {
            let view = pages[page];
            // console.log('view', view);
            // set up menu click handlers
            $('a[' + view.menuLink + ']').on('click', () => {
                views.changeView(view);
            });
        }
    }
};
