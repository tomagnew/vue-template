// Method 2: using a hashtable of usernames/passwords

let users = { // 
    'cliff': {
        name: 'Cliff Hales',
        password: 'password_for_cliff'
    },
    'tom': {
        name: 'Tom Agnew',
        password: 'password_for_tom',
    },
    'elaine': {
        name: 'Elaine Cochran',
        password: 'password_for_elaine',
    },
    'sun': {
        name: 'Immanuel Sun',
        password: 'password_for_sun'
    }
};

const AccessFactory = function (users) {
    this.get = () => {
        return {
            loggedIn: false,
            currentUser: null,
            login(username, password) {
                // verify credentials
                let user = users[username];
                if (user && user.password == password) {
                    this.loggedIn = true;
                    this.currentUser = {
                        username,
                        name:user.name
                    };
                    return true;
                } else {
                    this.loggedIn = false;
                    this.currentUser = null;
                }
                return false;
            }
        }
    }
};

export default new AccessFactory(users);
