var assert = require('chai').assert;  // use chai 'assert' assertion library
import AccessFactory from './method_2';

describe('Access Module: method_2 tests', () => {
    describe('Logging in with valid credentials', () => {
        // arrange
        let access = AccessFactory.get();
        const username = "tom";
        const password = "password_for_tom";
        // act
        const authenticated = access.login(username, password);
        // assert
        it('should change loginStatus to true', () => {
            assert.equal(access.loggedIn, true);
        });
        it('should change current user to authenticated user', () => {
            assert.equal(access.currentUser.username, username);
        });
        it('should return true to caller', () => {
            assert.equal(authenticated, true);
        });
    });
    describe('Logging in with invalid credentials', () => {
        // arrange
        let access = AccessFactory.get();
        const username = "fred";
        const password = "password_for_fred";
        // act
        const authenticated = access.login(username, password);
        // assert
        it('loginStatus should be false', () => {
            assert.equal(access.loggedIn, false);
        });
        it('current user should be null', () => {
            assert.equal(access.currentUser, null);
        });
        it('should return false to caller', () => {
            assert.equal(authenticated, false);
        });
    });
});