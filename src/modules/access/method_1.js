// Method 1: using an array of users

let users = [
    {
        username: 'cliff',
        name: 'Cliff Hales',
        password: 'pwdcliff'
    },
    {
        username: 'tom',
        name: 'Tom Agnew',
        password: 'pwdtom'
    },
    {
        username: 'elaine',
        name: 'Elaine Cochran',
        password: 'pwdelaine'
    },
    {
        username: 'sun',
        name: 'Immanuel Sun',
        password: 'pwdsun'
    }];

const access = {
    loggedIn: false,
    currentUser: null,
    login(username, password) {
        // verify credentials
        const user = users.find(u => u.username.toLowerCase() == username.toLowerCase() && u.password == password);
        if (user) { // user is found and authenticated
            this.loggedIn = true;
            this.currentUser = user;
            console.log("user logged in!");
            return true;
        }
        else { // user not authenticated 
            this.loginStatus = false;
            this.currentUser = null;
            console.log("login failed!");
        }
        return false;
    },
    logout() {
        this.loggedIn = false;
        this.currentUser = null;
        console.log("user logged out!");
    }
}

export default access;
