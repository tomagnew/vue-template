// Application Routes
// import Home from './components/home.vue';
import User from './components/user/user.vue';
import UserStart from './components/user/UserStart.vue';
import UserEdit from './components/user/UserEdit.vue';
import UserDetail from './components/user/UserDetail.vue';
import Home from './components/home/home.vue';
import About from './components/about/About.vue';
import Contact from './components/contact/Contact.vue';
import MemberArea from './components/member-area/MemberArea.vue';

export const routes = [
    { path: '', component: Home },
    { path: '/about', component: About },
    { path: '/contact', component: Contact },
    { path: '/members', component: MemberArea },
    {
        path: '/user', component: User, children: [
            { path: '', component: UserStart },
            { path: ':id', component: UserDetail },
            { path: ':id/edit', component: UserEdit },
        ]
    }
    // {
    //     path: '/user/:id', component: User, props: (route) => {
    //         console.log(route);
    //         return { id: route.params.id }
    //     }
    // }
];