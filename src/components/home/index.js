import $ from 'jquery';
import homePageHtml from 'html-loader!./home.html';

// load member area content to hidden div
$("#home_page").html(homePageHtml);

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$("#btn-contacts").on('click', () => {
    $.getJSON('https://randomuser.me/api/?results=50')
        .done(response => {
            console.log(response);
            let contacts = response.results;
            let $contacts = $(".contact-list");
            $contacts.append("<h2>Contact List</h2> <ul class='list-group'>");
            contacts.forEach((c, index) => {
                let fullName = jsUcfirst(c.name.title) + ' ' + jsUcfirst(c.name.first) + ' ' + jsUcfirst(c.name.last);
                let $contact = $(`
                <li class="list-group-item" data-id="${index}">${fullName}</li>
                `);
                $contact.on('click', (e) => {
                    let id = e.target.attributes['data-id'].value;
                    console.log(id, e);
                    loadContactDetails(id);
                    $('#detailModal').modal('show');
                }).on('mouseover', function (e) {
                    $(this).addClass('active');
                }).on('mouseout',function(e){
                    $(this).removeClass('active');
                });
                $contacts.append($contact);
                $contacts.append("</ul>");
                // console.log(fullName,index);
            });
            function loadContactDetails(id) {
                let contact = contacts[id];

                console.log(id, contact);
                let $contact = $(".contact-detail");
                $contact.html(`
                <div class="card">
                    <h2 class="card-header">Contact Details</h2>
                    <img class="card-img-top" style="height: auto; width: auto; display: block; margin-top: 2em;" src="${contact.picture.large}"/>
                    <div class="card-block">
                        <h3 class="card-title">${jsUcfirst(contact.name.title) + ' ' + jsUcfirst(contact.name.first) + ' ' + jsUcfirst(contact.name.last)}</h3>
                        <hr />

                         <ul class="list-group list-group-flush">
                                 <li class="list-group-item">
                                    <b>Gender:</b>
                                    <br>
                                    ${jsUcfirst(contact.gender)}</li>
                                <li class="list-group-item">
                                    <b>Address:</b>
                                    <br>
                                    ${jsUcfirst(contact.location.street)}
                                    <br>
                                    ${jsUcfirst(contact.location.city) + ', ' + jsUcfirst(contact.location.state) + ' ' + contact.location.postcode}
                                </li>
                                 <li class="list-group-item">
                                    <b>Phone:</b>
                                    <br>
                                    ${contact.phone}
                                 </li>
                                 <li class="list-group-item">
                                    <b>Email:</b>
                                    <br />
                                    ${contact.email}
                                 </li>
                        </ul>

                    </div> <!-- end of card-block -->
                </div> <!-- end of card -->
            `);
            };
        });
});



// exports function which shows member area if user logged in, hides otherwise
export default null;
