import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import { routes } from './routes';

console.log('application started!');
var bootstrap = require('bootstrap-loader');
global.$=require('jquery');
var styles = require('./js/styles');

Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode:'history'
});

global.vue = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})